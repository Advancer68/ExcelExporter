﻿namespace ExcelExporter
{
	public class FieldInfo : ILuaAnnation
	{
		public string name;//
		public string type;//
		public string desc;//
		public int idx;//

		public string ToAnnation()
		{
			return $"---@field public {name} {type} @{desc}";
		}

		public string ToLuaDefine(string value)
		{
			var str = type.ToLuaDefineStr(value);
			return $" {name} = {str},";
		}

	}
}