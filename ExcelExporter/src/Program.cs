﻿using System;
using System.IO;
using System.Linq;

namespace ExcelExporter
{

	public class ErrorFlag
	{
		public static bool hasError = false;

		public static void Add()
		{
			hasError = true;
		}
	}
	class Program
	{
		static void Main(string[] args)
		{

			//LetterToNumber("aa");
			//return;
			var curdir = Directory.GetCurrentDirectory();
#if LogPath
#endif
			Console.WriteLine("curdir:{0}", curdir);

			//var filePath = args[0];
			curdir = "d:\\Users\\ChenJia\\Documents\\Svn\\slgcommonTool\\excelConfig\\";
			var inpath = "";
			var outpath = "";
			if (args.Length >= 2)
			{
				inpath = args[0];
				outpath = args[1];
			}
			else if (args.Length >= 1)
			{
				inpath = args[0];
				outpath = Path.Combine(inpath, "lua");
			}
			else
			{
				inpath = curdir;
				outpath = Path.Combine(inpath, "lua");
			}
			var generator = new LuaGenerater()
			{
				inpath = inpath,
				outpath = outpath
			};

#if LogPath

			Console.WriteLine("inpath:{0}", inpath);
			Console.WriteLine("outpath:{0}", outpath);
#endif
			generator.GenerateDir(inpath);
			if (ErrorFlag.hasError)
			{
				Console.WriteLine("导出失败,详情查看日志");
				Console.ReadKey();
			}
			else
			{
				Console.WriteLine("导出成功!");
			}
		}

		public static long LetterToIndex(string str)
		{
			var v = LetterToNumber(str);
			if (v > 0)
			{
				v--;
			}

			return v;
		}

		public static long LetterToNumber(string str)
		{
			if (string.IsNullOrWhiteSpace(str))
			{
				return 0;
			}
			var lstr = str.ToLower().Trim();
			var b = 0;
			long num = 0;

			foreach (var it in lstr.Reverse())
			{
				int i = it;
				i -= 96;
				num += (long)Math.Pow(26, b) * i;
				//Console.WriteLine(i);
				b++;
			}
			//Console.WriteLine(num);
			//Console.ReadKey();

			return num;
		}
	}
}
