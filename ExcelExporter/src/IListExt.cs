﻿using System;
using System.Collections.Generic;

namespace ExcelExporter
{
	public static class IListExt
	{

		public static T GetOrNew<T>(this IList<T> self, int idx) where T : class, new()
		{
			try
			{
				return self[idx];
			}
			catch (Exception e)
			{
				//Console.WriteLine(e);
				//throw;
			}

			return null;
		}

		public static List<string> ToStringList<T>(this T[] self)
		{
			var list = new List<string>();
			foreach (var it in self)
			{
				list.Add(it.ToString());
			}

			return list;
		}


	}
}