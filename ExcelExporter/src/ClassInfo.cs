﻿using System.Collections.Generic;
using System.Text;

namespace ExcelExporter
{
	public class ClassInfo : ILuaAnnation
	{
		private static readonly StringBuilder bld = new StringBuilder(100);
		public string srcFile;//
		public string clsname;                                                      //
		public string desc;
		public string basecls = "baseobject";//
		public List<FieldInfo> Infos = new List<FieldInfo>();//
		public bool haskey = false;//

		public string clstype => haskey ? clsname + "Table" : clsname + "[]";

		public string ToAnnation()
		{
			bld.Clear();

			bld.AppendLine($"---parse form:{srcFile}");
			bld.AppendLine($"---@class {clsname} : {basecls} @{desc}");

			foreach (var it in Infos)
			{
				bld.AppendLine(it.ToAnnation());
			}

			bld.AppendFormat("local {0} = {1}", clsname, "{}");
			return bld.ToString();
		}

		public string ToLuaRequire()
		{
			return string.Format("c.{0} = require \'ExcelConfig.{0}\'", clsname);
		}


	}
}